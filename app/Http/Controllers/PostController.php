<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Validator;
class PostController extends Controller
{
    //
    public   function index(){
        $post = Post::all();
        return view ('post.index', compact('post'));
    }
    public function addPost(Request $request){
        $rules = array(
            'title' => 'required',
            'body'=> 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return respones::json(array('errors' =>getMessageBag()->toArray()));
        }else{
            $post = new Post;
            $post -> title = $request->title;
            $post -> body = $request->body;
            $post ->save();
            return response() -> json($post);
        }
        
    }

    public function editPost(Request $request){
        // dd($request->all());
        $post = Post::find($request->id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->save();
        return response()->json($post);
    }
    public function delePost(Request $request){
        $post = Post::find($request->id)->delete();
        return response()->json();
    }
}
