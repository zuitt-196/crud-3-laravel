<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/posts', 'PostController@index')->name('post.index');
Route::post('/add-post', 'PostController@addPost');
Route::post('/post/edit', 'PostController@editPost');
Route::post('/post/delete', 'PostController@delePost');

// //
// Route::group(['middleware' =>['web']], function(){
//     Route::resource('user', 'PostController');
//     Route::POST('addpost', 'postController@addPost');
// }) ;
