<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <title>@yield('title')</title>
</head>
<body>

    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarNav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('post.index') }}">Vhong-CRUD</a>
            </div>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="nav navbar-nav">
                    <!-- Add your navigation links here -->
                </ul>
            </div>
        </div>
    </nav>
    <div class="container mt-5">
        @yield('content')
    </div>

    <!-- Bootstrap JS -->
    
    

    {{-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    {{-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> --}}

    {{-- @yield('scripts') --}}

    <script type="text/javascript">
        // $(document).on('click', function(){
        //     $('#create').modal('show')
        //     $('.form-horizontal').show();
        //     $('.modal-title').text('Add post')
        // })
            
        $(document).ready(function(){
                $('.create-modal').on('click', function(){
                        $('#create').modal('show');
                        $('.form-horizontal').show();
                        $('.modal-title').text('Add post');
            
                    });

                    // function Add(save)
                        $('#add').click(function () {
                            // alert("sds")
                            $.ajax({
                            type: "POST",
                            url: 'add-post',
                            data: {
                            _token: '{{ csrf_token() }}',
                            title: $('#title').val(),
                            body: $('#body').val()
                        },
                        success: function(data) {
                            if (data.errors) {
                                $('.error').removeClass('hidden');
                                $('.error').text(data.errors.title);
                                $('.error').text(data.errors.body);
                            } else {
                                $('.error').remove();
                                $('#table').append("<tr class='post " + data.id + "'>" + 
                                    '<td>' + data.id + '</td>' +
                                    '<td>' + data.title + '</td>' +
                                    '<td>' + data.body + '</td>' +
                                    '<td>' + data.created_at + '</td>' +
                                        '<td><a class="show-modal btn btn-info btn-sm" data-id="' + data.id + '" data-title="' + data.title + '" data-body="' + data.body + '">' +
                                        '<i class="fa fa-eye"></i></a>' +
                                        '<a class="show-modal btn btn-warning btn-sm" data-id="' + data.id + '" data-title="' + data.title + '" data-body="' + data.body + '">' +
                                        '<i class="fa fa-edit"></i></a>' +
                                        '<a class="show-modal btn btn-warning btn-sm" data-id="' + data.id + '" data-title="' + data.title + '" data-body="' + data.body + '">' +
                                        '<i class="fa fa-trash-alt"></i></a>' +
                                      '</td>'
                                      

                                );
                                // AFTER CREATE  MUIST BE RESET 
                            $('#title').val('')
                            $('#body').val('')
                            $('#create').modal('hide');
                           
                    }}

                });
            })

            // SHOW Data THE  FUNCTION 
            $(document).on('click' , '.show-modal' , function () {
                // Get the data attributes from the clicked link
                let id = $(this).data('id');
                let title = $(this).data('title');
                let body = $(this).data('body');
                // Update the modal content with the data
                $("#modal-id").text( 'ID:' + " " + id);
                $('#modal-title').text( 'Title:' + " " +title);
                $('#modal-body').text('Body:' + " "  + body)
                 // Show the modal
                $('#showModal').modal('show');
            })

            // EDIT THE POST 
            $(document).on('click', '.edit-modal' , function () {
                $('#footer_action_button').text('Update Post');
                $('#footer_action_button').addClass('footer_action_button')
                $('.actionBtn').addClass('btn-success edit');
                $('.modal-title').text('Post edit');
                $('.deleteContent').hide();
                $('.form-horizontal').show();
                $('#fid').val($(this).data('id'));
                $('#id-title').val($(this).data('title'));
               $('#id-body').val($(this).data('body'));
               $('#myModal').modal('show');
                })

                $('.modal-footer').on('click', '.edit', function () {
    
                    $.ajax({
                    type: "POST",
                    url: '/post/edit',
                data: {
                    _token: '{{ csrf_token() }}',
                    id: $('#fid').val(),
                    title: $('#id-title').val(),
                    body: $('#id-body').val(),
                },
            success: function(data) {
                $('.post' + data.id).replaceWith(
                    "<tr class='post" + data.id + "'>" +
                    "<td>" + data.id + "</td>" +
                    "<td>" + data.title + "</td>" +
                    "<td>" + data.body + "</td>" +
                    '<td>' + data.created_at + '</td>' +
                    '<td><a class="show-modal btn btn-info btn-sm" data-id="' + data.id + '" data-title="' + data.title + '" data-body="' + data.body + '">' +
                    '<i class="fa fa-eye"></i></a>' +
                    '<a class="show-modal btn btn-warning btn-sm" data-id="' + data.id + '" data-title="' + data.title + '" data-body="' + data.body + '">' +
                    '<i class="fa fa-edit"></i></a>' +
                    '<a class="show-modal btn btn-warning btn-sm" data-id="' + data.id + '" data-title="' + data.title + '" data-body="' + data.body + '">' +
                    '<i class="fa fa-trash-alt"></i></a>' +
                    '</td>' +
                    "</tr>"
                );
            }
            });
          })

        //   DELETE THE DATA 
        $(document).on('click', '.delete-modal', function (p) {
                $('#footer_action_button').text('Delete Post');
                $('#footer_action_button').removeClass('fas fa-times')
                $('.actionBtn').addClass('btn-success fa-trash-alt');
                $('.modal-title').text('Delete edit');
                $('.deleteContent').show();
                $('.form-horizontal').hide();
                $('#fid').val($(this).data('id'));
                $('#id-title').val($(this).data('title'));
               $('#id-body').val($(this).data('body'));
               $('#myModal').modal('show');
        })


        $('.modal-footer').on('click', '.delete', function () {
                $.ajax({
                    type: "POST",
                    url:'/post/delete',
                    data:{
                        _token: '{{ csrf_token() }}',
                        id: $('.id').text()
                    },
                    success:function(data){
                        $('.post' + data.id.remove())
                    }
                })
        })
});

</script>
</body>
</html>