@extends('layout.app')
@section('content')
<div class ="row">
    <div  class="col-md-12">
        <h2>Simple CRUD AJAX</h2>
    </div>
</div>



<div class="row">
    <div class="table table-responsive">
        <table class="table table-bordered " id="table"> 
            <tr>
                <th width="150px">No</th>
                <th>Title</th>
                <th>Body</th>
                <th>Create At</th>

                <th class="text-center" width="150px"> 
                    <a href="#" class="create-modal btn btn-success btn-sm">
                        <i class="fas fa-plus"></i>
                    </a>
                </th>
            </tr>

            {{ csrf_field() }}

            <?php   $no=1; ?>   

            @foreach ($post as $key => $value)

        <tr class="post {{ $value->id }}">
            <td>{{ $no++ }}</td>
            <td>{{ $value->title }}</td>
            <td>{{ $value->body }}</td>
            <td>{{ $value->created_at }}</td>
            <td>
                <a href="#" class="show-modal btn btn-info btn-sm" data-id="{{ $value->id }}" data-title="{{ $value->title }}" data-body="{{ $value->body }}">
                    <i class="fa fa-eye"></i>
                </a>
                <a href="#" class="edit-modal btn btn-warning btn-sm" data-id="{{ $value->id }}" data-title="{{ $value->title }}" data-body="{{ $value->body }}">
                    <i class="fas fa-edit"></i>
                </a>
                    <a href="#" class="delete-modal btn btn-warning btn-sm" data-id="{{ $value->id }}" data-title="{{ $value->title }}" data-body="{{ $value->body }}">
                        <i class="fas fa-trash-alt"></i>
                    </a>
            </td>
        </tr>
    @endforeach

      </table>
    </div>
</div>

{{-- CREATE FORM  POST --}}
<div id="create" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="create">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" >
                    {{-- TITLE  --}}
                    <div class="form-group row add">
                        <label for="title" class="control-label col-sm-2">Title:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" id="title" placeholder="Your title Here" required>
                            <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    {{-- BODY --}}
                    <div class="form-group">
                        <label for="body" class="control-label col-sm-2">Body:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="body" id="body" placeholder="Your body Here" required>
                            <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" name="button" id="add" class="btn btn-warning">
                    <i class="fas fa-check"></i> Save
                </button>

                <button type="button" name="button" id="add" class="btn btn-warning" data-dismiss="modal">
                    <i class="fas fa-times"></i>Close
                </button>
            </div>
        </div>
    </div>
</div>

{{-- SHOW FORM POST --}}
<div id="showModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                {{-- @foreach ($post as $key => $value) --}}
                <p id="modal-id"></p>
                <p id="modal-title">Title</p>
                <p id="modal-body">Body</p>
             
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" data-dismiss="modal">
                    <i class="fas fa-times"></i>Close
                </button>
            </div>
        </div>  
    </div>
</div>

{{-- FORM EDIT AND DELETE --}}
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2">ID</label>
                        <div class="col-sm-10">   
                            <input type="text" class="form-control" id="fid" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id-title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Body</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="id-body"></textarea>
                        </div>
                    </div>
                </form>
                <div class="deleteContent">
                    Are you sure you want to delete <span class="title"></span>?
                    <span class="hidden id"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn actionBtn" data-dismiss="modal">
                    <i id="footer_action_button" class="fas fa-edit"></i>
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i id="footer_action_button" class="fas fa-times"></i>
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

